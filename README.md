# VideoPoker

Project uses additional Package "TextMesh Pro"

Added tests for HandAnalyzer class - EditMode (others are in progress)

## HOW TO PLAY

Download ZIP from https://bitbucket.org/hally/videopoker/downloads/ and unpack it

Run **VideoPokerJacksOrBetter.exe**

1. Choose a bet size using arrow buttons in the right action bar

2. Press DEAL button to get first deal, Credits amount is updated at the same time according to chosen bet size

3. Define possible winning combination and click on the cards to hold chosen

4. Press DEAL button to get second deal. All cards which have not been held in previous round will be replaced from a deck

5. See text message for the result

6. Win amount is assigned according to the winning combination (row) and a choosen bet size (column) from the PayoutTable

7. Credits amount increases when there is a win

8. If want to continue repeat steps 1-7, otherwise close the app
